#!/usr/bin/env python

from binary_tree import *
from unittest import main, TestCase

def createTestTree(labels):
    if len(labels) != 5:
        raise ValueError('length not equal to 5')
    nodes = [Node(label) for label in labels]
    nodes[0].setChildren(nodes[1], nodes[4])
    nodes[1].setChildren(nodes[2], nodes[3])
    return nodes[0]

lemonTree = createTestTree('lemon')
mangoTree = createTestTree('mango')

class BinaryTreeTest(TestCase):

    def testPreorderTraversal(self):
        #expect:
        self.assertEqual(list('lemon'), lemonTree.traversePreorder())
        self.assertEqual(list('mango'), mangoTree.traversePreorder())

    def testInorderTraversal(self):
        #expect:
        self.assertEqual(list('meoln'), lemonTree.traverseInorder())
        self.assertEqual(list('nagmo'), mangoTree.traverseInorder())

    def testPostorderTraversal(self):
        #expect:
        self.assertEqual(list('moenl'), lemonTree.traversePostorder())
        self.assertEqual(list('ngaom'), mangoTree.traversePostorder())

class BinaryTreeClimberTest(TestCase):

    lemonClimber = BinaryTreeClimber(lemonTree)
    mangoClimber = BinaryTreeClimber(mangoTree)

    def testPreorderTraversal(self):
        #expect:
        self.assertEqual(list('lemon'), self.lemonClimber.traversePreorder())
        self.assertEqual(list('mango'), self.mangoClimber.traversePreorder())

    def testInorderTraversal(self):
        #expect:
        self.assertEqual(list('meoln'), self.lemonClimber.traverseInorder())
        self.assertEqual(list('nagmo'), self.mangoClimber.traverseInorder())

    def testPostorderTraversal(self):
        #expect:
        self.assertEqual(list('moenl'), self.lemonClimber.traversePostorder())
        self.assertEqual(list('ngaom'), self.mangoClimber.traversePostorder())

if __name__ == '__main__':
    main()
