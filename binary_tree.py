#!/usr/bin/env python

class Node(object):

    def __init__(self, label, leftChild=None, rightChild=None):
        self.label = label
        self.setChildren(leftChild, rightChild)

    def setChildren(self, leftChild=None, rightChild=None):
        self.leftChild = leftChild
        self.rightChild = rightChild

    def visit(self):
        return self.label

    def traversePreorder(self):
        traversal = [self.visit()]
        if self.leftChild != None:
            traversal += self.leftChild.traversePreorder()
        if self.rightChild != None:
            traversal += self.rightChild.traversePreorder()
        return traversal

    def traverseInorder(self):
        traversal = []
        if self.leftChild != None:
            traversal += self.leftChild.traverseInorder()
        traversal.append(self.visit())
        if self.rightChild != None:
            traversal += self.rightChild.traverseInorder()
        return traversal

    def traversePostorder(self):
        traversal = []
        if self.leftChild != None:
            traversal += self.leftChild.traversePostorder()
        if self.rightChild != None:
            traversal += self.rightChild.traversePostorder()
        traversal.append(self.visit())
        return traversal

class BinaryTreeClimber(object):

    def __init__(self, tree=None):
        self.tree = tree

    def traversePreorder(self):
        traversal = []
        stack = [self.tree]
        while len(stack) != 0:
            node = stack.pop()
            traversal.append(node.visit())
            if node.rightChild != None:
                stack.append(node.rightChild)
            if node.leftChild != None:
                stack.append(node.leftChild)
        return traversal

    def traverseInorder(self):
        traversal = []
        stack = [self.tree]
        while len(stack) > 0:
            node = stack.pop()
            if node.rightChild != None:
                stack.append(node.rightChild)
            if node.leftChild != None:
                stack.append(Node(node.label))
                stack.append(node.leftChild)
            else:
                traversal.append(node.visit())
        return traversal

    def traversePostorder(self):
        traversal = []
        stack = [self.tree]
        while len(stack) > 0:
            node = stack.pop()
            if node.rightChild != None:
                stack.append(Node(node.label))
                stack.append(node.rightChild)
            elif node.leftChild == None:
                traversal.append(node.label)
            if node.leftChild != None:
                stack.append(node.leftChild)
        return traversal
